package org.company

class Employee {
    String name
    String position
    Double salary
    Integer phoneNumber
    String email
    String dob

      static constraints = {
          name blank: false
          position inList: ["Clerk","Manager","Supervisor"], blank: false
          salary blank: false, range: 900..2300
          phoneNumber blank: false, unique:true, matches: '^(\\(?\\+?[0-9]*\\)?)?[0-9_\\- \\(\\)]*$'
          email email:true, blank: false, unique:true
        }
}
