package org.company

import grails.transaction.Transactional

@Transactional(readOnly = false)
class EmployeeController {

  def index() {
    def employee = Employee.list()

       [emp:employee]
  }

  def create() {
    respond new Employee(params)
  }

  @Transactional
  def save(Employee employee) {
    if (employee == null) {
            notFound()
            return
        }

    if (employee.hasErrors()) {
            respond employee.errors, view:'create'
            return
        }

    employee.save flush:true
    redirect action: 'index'
  }

  def edit(Employee employee) {
    respond employee
  }

  @Transactional
  def update(Employee employee) {
    if (employee == null) {
        notFound()
        return
    }

    if (employee.hasErrors()) {
        respond employee.errors, view:'edit'
        return
    }
    employee.save flush:true
    redirect action: 'index'
  }

  def show(Employee employee) {
    respond employee
  }

  @Transactional
  def delete(Employee employee) {
    if (employee == null) {
            notFound()
            return
        }

    employee.delete flush:true
    redirect action: 'index'
  }

  protected notFound(){
    redirect action: "index"
  }
}
