<!DOCTYPE html>
<html>
  <head>
    <title><g:layoutTitle default="Employee Management System"/></title>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

      <g:layoutHead/>
    </head>

    <body style="padding-bottom:50em">

      <div class="" >
        <div class="navbar-static-top">
          <nav class="navbar navbar-default">
            <div class="container-fluid">
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/#"><span class="glyphicon glyphicon-user"></span> Employee Management System</a>
              </div>
              <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                  <li class="">
                    <a href="/#">Home
                      <span class="sr-only">(current)</span>
                    </a>
                  </li>
                  <li class="">
                    <a href="/employee/create">Add Employee</a>
                  </li>
                  <li class="">
                    <a href="/employee/index">Employee List</a>
                  </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                  <li class="hidden-xs">
                    <a>
                      <span class="text-info">${println(new Date())}</span>
                    </a>
                  </li>
                </ul>
              </div>
              <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
          </nav>
        </div>
      </div>

      <g:layoutBody/>
    </body>

    <footer class="footer navbar-fixed-bottom">
      <div style="background:white">
        <div class="container">
          <p>Created by
            <a href="fb.com/mr.shahlol" style="text-decoration: none">Abdulsalam Shahlol</a>
          </p>
        </div>
      </div>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </footer>
  </html>
