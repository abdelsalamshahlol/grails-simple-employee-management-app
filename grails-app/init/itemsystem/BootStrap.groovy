package itemsystem
import com.item.Car

class BootStrap {
    def init = { servletContext ->
        if (Car.count() == 0) {
      new Car(year: '2007', model: 'Accent GLS', make: 'Hyundai', date: new Date()).save()
      new Car(year: '2015', model: 'Vios', make: 'Toyota', date: new Date()).save()
      new Car(year: '2017', model: 'Golf Gti', make: 'VW', date: new Date()).save()
      new Car(year: '2011', model: 'gt86', make: 'Toyota', date: new Date()).save()
      new Car(year: '2017', model: 'Gensis Coupe', make: 'Hyundai', date: new Date()).save()
      new Car(year: '2016', model: 'Sonata', make: 'Hyundai', date: new Date()).save()
      new Car(year: '2013', model: 'Tacoma', make: 'Toyota', date: new Date()).save()
    }
}
    def destroy = {
    }
}
