<!DOCTYPE html>
<html>
  <head>
    <meta name="layout" content="master"/>
    <title>Employee Index</title>
  </head>
  <body>
    <div class="col-md-8 col-md-offset-2 ">
      <legend>${employee.name} details</legend>
        <div class="row" style="height:2em">
          <label class="col-md-4" for="" class="col-md-4">Name</label>
          <p  class="col-md-4" style="font-size:20px">${employee.name}</p>
        </div><hr>
        <div class="row" style="height:2em">
          <label class="col-md-4" for="">Position</label>
          <p class="col-md-4" style="font-size:20px">${employee.position}</p>
        </div><hr>
        <div class="row" style="height:2em">
          <label class="col-md-4" for="">Salary</label>
          <p class="col-md-4" style="font-size:20px">${employee.salary}</p>
        </div><hr>
        <div class="row" style="height:2em">
          <label class="col-md-4" for="">Phone Number</label>
          <p class="col-md-4" style="font-size:20px">${employee.phoneNumber}</p>
        </div><hr>
        <div class="row" style="height:2em">
          <label class="col-md-4" for="">Email</label>
          <p class="col-md-4" style="font-size:20px">${employee.email}</p>
        </div><hr>
        <div class="row" style="height:2em">
          <label class="col-md-4" for="">Date of Birth</label>
          <p class="col-md-4" class="col-md-4" style="font-size:20px">${employee.dob}</p>
        </div><hr>
        <a href="/employee/edit/${employee.id}" class="btn btn-warning">Update</a>
        <g:form resource="${this.employee}" method="DELETE" style="display:inline">
        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you want to delete ${employee.name} sure?');">Delete</a>
      </g:form>
      </div>
  </body>
</html>
