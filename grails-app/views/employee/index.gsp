<!DOCTYPE html>
<html>
  <head>
    <meta name="layout" content="master"/>
    <title>Employee Index</title>
  </head>
  <body>
    <div class="col-md-8 col-md-offset-2">
      <legend>List of Employees</legend>
      <table class="table table-bordered table-hover">
        <thead>
          <tr>
            <td>
              <b>No.</b>
            </td>
            <td>
              <b>Name</b>
            </td>
            <td>
              <b>Position</b>
            </td>
            <td>
              <b>Salary</b>
            </td>
            <td>
              <b>Phone Number</b>
            </td>
            <td>
              <b>Email</b>
            </td>
            <td>
              <b>Date of Birth</b>
            </td>
            <td>
              <b>Action</b>
            </td>
          </tr>
        </thead>
        <tbody>
          <g:each in="${emp}" var="p" status="i">
            <tr>
              <td>${i+1}</td>
              <td>${p.name}</td>
              <td>${p.position}</td>
              <td>${p.salary}</td>
              <td>${p.phoneNumber}</td>
              <td>${p.email}</td>
              <td>${p.dob}</td>
              <td>
                <a href="/employee/show/${p.id}" class="btn btn-xs btn-primary">Show</a>
              </td>
            </tr>
          </g:each>
        </tbody>
      </table>
    </div>
  </body>
</html>
