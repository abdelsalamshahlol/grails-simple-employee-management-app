package com.item

class Car {
  String year
  String model
  String make
  Date date = new Date()

    static constraints = {
      year  blank: false
      model  blank: false
      make (inList:['Toyota', 'Hyundai', 'VW'])
    }
}
