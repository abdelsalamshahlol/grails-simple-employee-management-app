<!DOCTYPE html>
<html>
  <head>
    <meta name="layout" content="master"/>
    <title>Add Employee</title>
  </head>
  <body>
    <div class="col-md-8 col-md-offset-2">

      <g:form resource="${this.employee}" class="form-horizontal" method="PUT">
        <fieldset>

          <!-- Form Name -->
          <legend>Edit ${employee.name} details</legend>

          <!-- Text input-->
          <div class="form-group">
            <label class="col-md-4 control-label" for="name">Name</label>
            <div class="col-md-4">
              <input id="name" name="name" type="text" value="${employee.name ?: ''}" class="form-control input-md" required></div>
            </div>

            <!-- Select Basic -->
            <div class="form-group">
              <label class="col-md-4 control-label" for="position">Position</label>
              <div class="col-md-4">
                <select required id="position" name="position" class="form-control">
                  <option value="Clerk">Clerk</option>
                  <option value="Manager">Manager</option>
                  <option value="Supervisor">Supervisor</option>
                </select>
              </div>
            </div>

            <!-- Text input-->
            <div class="form-group">
              <label class="col-md-4 control-label" for="salary">Salary</label>
              <div class="col-md-4">
                <input id="salary" name="salary" type="text" value="${employee.salary ?: ''}" class="form-control input-md"></div>
              </div>

              <!-- Text input-->
              <div class="form-group">
                <label class="col-md-4 control-label" for="phoneNumber">Phone Number</label>
                <div class="col-md-4">
                  <input id="phoneNumber" name="phoneNumber" type="text" value="${employee.phoneNumber ?: ''}" class="form-control input-md"></div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                  <label class="col-md-4 control-label" for="email">Email</label>
                  <div class="col-md-4">
                    <input id="email" name="email" type="text" value="${employee.email ?: ''}" class="form-control input-md"></div>
                  </div>

                  <!-- Text input-->
                  <div class="form-group">
                    <label class="col-md-4 control-label" for="dob">Date of Birth</label>
                    <div class="col-md-4">
                      <input id="dob" name="dob" type="date" value="${employee.dob ?: ''}" max="1997-12-31" min="1970-1-1" placeholder="" class="form-control input-md"></div>
                    </div>

                    <!-- Button -->
                    <div class="form-group">
                      <label class="col-md-4 control-label" for=""></label>
                      <div class="col-md-4">
                        <button id="" name="" class="btn btn-primary">Submit</button>
                      </div>
                    </div>

                  </fieldset>

                </g:form>
                <g:hasErrors bean="${this.employee}" class="panel panel-danger">
                  <div class="panel panel-danger">
                    <ul class="errors" role="alert">
                      <g:eachError bean="${this.employee}" var="error"><li class="text-danger" <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                  </g:eachError>
                </ul>
              </div>
            </g:hasErrors>
          </div>
        </body>
      </html>
